# 实验4：PL/SQL语言打印杨辉三角

学号：202010424211	 姓名：廖启航	班级：软件3班

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```

- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。

```
create or replace procedure yhtriange (n in integer) is
  type t_number is varray(100) of integer not null; --数组
  i integer;
  j integer;
  spaces varchar2(30) := '   ';--三个空格，用于打印时分隔数字
  rowarray t_number := t_number();
begin
  dbms_output.put_line('1'); --先打印第1行
  dbms_output.put(rpad(1, 9, ' ')); --先打印第2行
  dbms_output.put(rpad(1, 9, ' ')); --打印第一个1
  dbms_output.put_line(''); --打印换行

  --初始化数组数据
  for i in 1 .. n loop
    rowarray.extend;
  end loop;
  
  rowarray(1) := 1;
  rowarray(2) := 1;

  for i in 3 .. n --打印每行，从第3行起
  loop
    rowarray(i) := 1;
    j := i - 1;
    
        --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
        --这里从第j-1个数字循环到第2个数字，顺序是从右到左
    while j > 1 loop
      rowarray(j) := rowarray(j) + rowarray(j - 1);
      j := j - 1;
    end loop;
    
    --打印第i行
    for j in 1 .. i loop
      dbms_output.put(rpad(rowarray(j), 9, ' ')); --打印第一个1
    end loop;
  
    dbms_output.put_line(''); --打印换行
  end loop;

end;
/
```

![image-20230510161422347](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230510161422347.png)

![image-20230510161515477](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230510161515477.png)

- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

```
exec yhtriange(9);
```



![image-20230510161826613](C:\Users\Administrator\AppData\Roaming\Typora\typora-user-images\image-20230510161826613.png)

## 总结

在实验过程中，我首先阅读了提供的杨辉三角源代码。杨辉三角是一个经典的数学问题，通过该实验我进一步理解了该问题的解决思路和算法。代码中使用了 PL/SQL 语言编写，包括变量的声明和使用、循环结构、条件判断等基本语法元素。

接着，我按照实验指导进行了源代码的运行。通过执行代码，我得到了杨辉三角的输出结果。观察结果，我更加清楚地理解了杨辉三角的形成规律和特点。

通过这次实验，我对 Oracle PL/SQL 语言有了更深入的了解。我学会了如何编写存储过程，以及如何在 PL/SQL 中使用变量、循环和条件语句等基本语法。
