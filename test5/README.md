# 实验5：包，过程，函数的用法

202010424211  廖启航

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```



## 脚本代码

```sql
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
SQL> create or replace PACKAGE MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;

程序包已创建。

SQL> create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/

程序包体已创建。
```

## 测试

```sh

函数Get_SalaryAmount()测试：
SQL> select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	   10 Administration			     4400
	   20 Marketing 			    19000
	   30 Purchasing			    24900
	   40 Human Resources			     6500
	   50 Shipping				   156400
	   60 IT				    28800
	   70 Public Relations			    10000
	   80 Sales				   304500
	   90 Executive 			    58000
	  100 Finance				    51608
	  110 Accounting			    20308

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  120 Treasury
	  130 Corporate Tax
	  140 Control And Credit
	  150 Shareholder Services
	  160 Benefits
	  170 Manufacturing
	  180 Construction
	  190 Contracting
	  200 Operations
	  210 IT Support
	  220 NOC

DEPARTMENT_ID DEPARTMENT_NAME		     SALARY_TOTAL
------------- ------------------------------ ------------
	  230 IT Helpdesk
	  240 Government Sales
	  250 Retail Sales
	  260 Recruiting
	  270 Payroll

已选择 27 行。


过程Get_Employees()测试：
SQL> set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/SQL> 
101 Neena
108 Nancy
109 Daniel
110 John
111 Ismael
112 Jose Manuel
113 Luis
200 Jennifer
203 Susan
204 Hermann
205 Shelley
206 William

PL/SQL 过程已成功完成。
```

## 实验总结与体会

本次实验主要目的是学习 PL/SQL 语言结构，了解变量和常量的声明和使用方法，并学习包、过程和函数的用法。实验内容包括创建包、编写函数和过程，以实现特定功能。

在实验过程中，我按照步骤顺利完成了实验任务。首先，我创建了一个名为 "MyPack" 的包。在该包中，我实现了一个函数 "Get_SalaryAmount"，它接受部门ID作为输入参数，并通过查询员工表统计每个部门的工资总额。这个函数的编写让我更深入地理解了 PL/SQL 中的变量声明、游标的使用以及聚合函数的应用。

接着，我在 "MyPack" 包中创建了一个过程 "GET_EMPLOYEES"。这个过程接受员工ID作为输入参数，并使用游标递归查询某个员工及其所有下属和子下属员工。通过这个过程的编写，我学会了如何在 PL/SQL 中使用游标来处理结果集，以及如何实现递归查询的功能。

