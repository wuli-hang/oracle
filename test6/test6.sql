create table TB_USER
(
  id   INTEGER,
  no   VARCHAR2(50),
  pwd  VARCHAR2(50),
  name VARCHAR2(50),
  type CHAR(1)
)tablespace demo

create table TB_CLASS
(
  id   INTEGER,
  name VARCHAR2(50)
)tablespace myDemo_temp

create table TB_COMMODITY
(
 id      INTEGER,
 product_name VARCHAR2(100),
 classify_id INTEGER,
 model    VARCHAR2(50),
 unit     VARCHAR2(50),
 market_value VARCHAR2(50),
 sales_price VARCHAR2(50),
 cost_price  VARCHAR2(50),
 img     VARCHAR2(100),
 introduce  VARCHAR2(500),
 num     INTEGER
)tablespace myDemo_temp

create table TB_CUSTOMER1
(
 id     INTEGER,
 customer_no VARCHAR2(50),
 name    VARCHAR2(50),
 phone    VARCHAR2(20),
 address   VARCHAR2(100)
)tablespace myDemo_temp

CREATE TABLE TB_ORDER1 (
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
price NUMBER(10,2),
CONSTRAINT order_details_primary_key PRIMARY KEY (order_id, product_id)
) TABLESPACE myDemo_temp;

--向用户表插入信息
INSERT INTO TB_USER VALUES ('01', 'admin', '123', 'admin', '1');
INSERT INTO TB_USER VALUES ('02', 'customer01', '123', 'customer02', '2');
INSERT INTO TB_USER VALUES ('02', 'customer02', '123', 'customer02', '3');
INSERT INTO TB_USER VALUES ('02', 'customer03', '123', 'customer02', '4');

select * from TB_USER


--商品分类插入数据


-- 使用循环插入随机生成的数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..10000 LOOP
    -- 生成随机的零食种类
    DECLARE
      random_category VARCHAR2(50);
    BEGIN
      SELECT '零食种类' || TO_CHAR(DBMS_RANDOM.VALUE(1, 100)) INTO random_category FROM DUAL;
      
      -- 执行插入操作
      INSERT INTO TB_CLASS (id, name) VALUES (snacks_seq.NEXTVAL, random_category);
    END;
  END LOOP;
  COMMIT;
END;
/


-- 插入数据
DECLARE
  v_name VARCHAR2(10);
  v_phone VARCHAR2(20);
BEGIN
  FOR i IN 1..3000 LOOP
    -- 生成随机的姓名
    SELECT CASE MOD(i, 3)
      WHEN 1 THEN '小明'
      WHEN 2 THEN '小红'
      ELSE '小王'
    END INTO v_name FROM DUAL;
    
    -- 生成随机的电话号码
    SELECT '1' || LPAD(TRUNC(DBMS_RANDOM.VALUE(100000000, 999999999)), 9, '0') INTO v_phone FROM DUAL;
    
    -- 执行插入操作
    INSERT INTO TB_CUSTOMER1 (name, phone, address)
    VALUES (v_name, v_phone, '成都大学');
  END LOOP;
  
  COMMIT;
END;
/



---插入下单数据
-- 创建表
CREATE TABLE TB_ORDERS (
  customer VARCHAR2(50),
  pro_name VARCHAR2(50),
  time TIMESTAMP,
  num NUMBER
);

-- 插入数据
DECLARE
  v_customer VARCHAR2(10);
  v_pro_name VARCHAR2(50);
  v_time TIMESTAMP;
  v_num NUMBER;
BEGIN
  FOR i IN 1..10000 LOOP
    -- 生成随机的用户姓名
    SELECT CASE MOD(i, 3)
      WHEN 1 THEN '小明'
      WHEN 2 THEN '小红'
      ELSE '小王'
    END INTO v_customer FROM DUAL;
    





-- 生成随机的商品名称
    SELECT '零食品种' || TO_CHAR(DBMS_RANDOM.VALUE(1, 1000)) INTO v_pro_name FROM DUAL;
    
    -- 生成随机的下单时间（精确到分钟）
    SELECT TO_TIMESTAMP('2023-01-01', 'YYYY-MM-DD') + INTERVAL '1' MINUTE * TRUNC(DBMS_RANDOM.VALUE(0, 365 * 24 * 60)) INTO v_time FROM DUAL;
    
    -- 生成随机的下单数量
    SELECT TRUNC(DBMS_RANDOM.VALUE(1, 100)) INTO v_num FROM DUAL;
    
    -- 执行插入操作
    INSERT INTO TB_ORDERS (customer, pro_name, time, num)
    VALUES (v_customer, v_pro_name, v_time, v_num);
  END LOOP;
  
  COMMIT;
END;
/
select * from TB_ORDERS





-- 创建管理员用户
CREATE USER MyAdmin01 IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE, DBA TO MyAdmin;

-- 创建普通用户
CREATE USER MyUser01 IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_CLASS TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_CUSTOMER1 TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_ORDERS TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_COMMODITY TO myUser01;



-- 创建程序包
CREATE OR REPLACE PACKAGE myORDER AS
  -- 存储过程：创建订单
  PROCEDURE create_order(
    p_customer VARCHAR2,
    p_pro_name VARCHAR2,
    p_time DATE,
    p_num NUMBER
  );

  -- 存储过程：计算订单总金额
  FUNCTION calculate_total_amount(
    p_order_id NUMBER
  ) RETURN NUMBER;
END myORDER;
/

-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY myORDER AS
  -- 存储过程：创建订单
  PROCEDURE create_order(
    p_customer VARCHAR2,
    p_pro_name VARCHAR2,
    p_time DATE,
    p_num NUMBER
  ) AS
  BEGIN
    INSERT INTO TB_ORDERS05 (id, customer, pro_name, time, num)
    VALUES (TB_ORDERS05_SEQ.NEXTVAL, p_customer, p_pro_name, p_time, p_num);
  END create_order;

  -- 存储过程：计算订单总金额
  FUNCTION calculate_total_amount(
    p_order_id NUMBER
  ) RETURN NUMBER AS
    v_total_amount NUMBER;
  BEGIN
    SELECT num * total_price INTO v_total_amount
    FROM TB_ORDERS05
    WHERE id = p_order_id;

    RETURN v_total_amount;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN 0;
  END calculate_total_amount;
END myORDER;
/

