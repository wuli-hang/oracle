﻿﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

姓名：廖启航	班级：20软工3班	学号：202010424211

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 项目概述

项目背景：**基于Oracle的零食销售系统数据库**

随着人们生活水平的提高和生活节奏的加快，零食作为一种方便、美味的食品，在市场中具有广泛的需求。为了满足零食销售行业的发展需求，提高销售效率和管理水平，设计和实施一个基于Oracle的零食销售系统数据库成为必要。

传统的零食销售方式主要依赖于实体店铺，面临着一些挑战，如库存管理困难、订单处理效率低下、数据统计分析复杂等问题。因此，基于Oracle的零食销售系统数据库的建设成为了一个迫切的需求。

该项目旨在建立一个高效、可靠的数据库系统，用于存储和管理零食销售系统的相关数据。通过该数据库系统，可以实现订单管理、产品管理、库存管理等核心业务功能，提供准确的数据查询和报表分析功能，帮助管理者做出科学决策。

通过零食销售系统数据库的建设，可以实现以下优势和益处：

- 提高销售效率：通过系统化的订单管理和库存管理，提高订单处理效率，缩短客户等待时间，提升销售效率。
- 提升客户体验：实现在线订购、快速结账和订单跟踪等功能，提供便捷、个性化的购物体验，增加客户黏性。
- 优化库存管理：通过数据库系统的库存管理功能，及时了解产品库存情况，避免库存过多或不足的问题，提高资金利用效率。
- 数据分析与决策支持：通过数据库系统提供的数据查询和报表分析功能，进行销售统计分析、产品热销分析等，帮助管理者制定科学的营销策略和决策。

综上所述，基于Oracle的零食销售系统数据库的建设将为零食销售行业提供一种高效、可靠的管理解决方案，帮助企业实现销售优化、客户满意度提升和业务发展的目标

### 表设计

```sql
--创建用户表
create table TB_USER
(
  id   INTEGER,
  no   VARCHAR2(50),
  pwd  VARCHAR2(50),
  name VARCHAR2(50),
  type CHAR(1)
)tablespace demo


```

```sql
--创建商品分类表：
create table TB_CLASS
(
  id   INTEGER,
  name VARCHAR2(50)
)tablespace myDemo_temp

```

```
--创建商品表：

create table TB_COMMODITY
(
 id      INTEGER,
 product_name VARCHAR2(100),
 classify_id INTEGER,
 model    VARCHAR2(50),
 unit     VARCHAR2(50),
 market_value VARCHAR2(50),
 sales_price VARCHAR2(50),
 cost_price  VARCHAR2(50),
 img     VARCHAR2(100),
 introduce  VARCHAR2(500),
 num     INTEGER
)tablespace myDemo_temp

```

```
--创建客户表：
create table TB_CUSTOMER1
(
 id     INTEGER,
 customer_no VARCHAR2(50),
 name    VARCHAR2(50),
 phone    VARCHAR2(20),
 address   VARCHAR2(100)
)tablespace myDemo_temp

```

```
--创建商品订单表：
CREATE TABLE TB_ORDER1 (
order_id NUMBER(10),
product_id NUMBER(10),
quantity NUMBER(10),
price NUMBER(10,2),
CONSTRAINT order_details_primary_key PRIMARY KEY (order_id, product_id)
) TABLESPACE myDemo_temp;

```

```
插入数据：

客户表
--向用户表插入信息
INSERT INTO TB_USER VALUES ('01', 'admin', '123', 'admin', '1');
INSERT INTO TB_USER VALUES ('02', 'customer01', '123', 'customer02', '2');
INSERT INTO TB_USER VALUES ('02', 'customer02', '123', 'customer02', '3');
INSERT INTO TB_USER VALUES ('02', 'customer03', '123', 'customer02', '4');

select * from TB_USER

```

```
插入商品：
--商品分类插入数据


-- 使用循环插入随机生成的数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..10000 LOOP
    -- 生成随机的零食种类
    DECLARE
      random_category VARCHAR2(50);
    BEGIN
      SELECT '零食种类' || TO_CHAR(DBMS_RANDOM.VALUE(1, 100)) INTO random_category FROM DUAL;
      
      -- 执行插入操作
      INSERT INTO TB_CLASS (id, name) VALUES (snacks_seq.NEXTVAL, random_category);
    END;
  END LOOP;
  COMMIT;
END;
/
```

```
插入用户数据

-- 插入数据
DECLARE
  v_name VARCHAR2(10);
  v_phone VARCHAR2(20);
BEGIN
  FOR i IN 1..3000 LOOP
    -- 生成随机的姓名
    SELECT CASE MOD(i, 3)
      WHEN 1 THEN '小明'
      WHEN 2 THEN '小红'
      ELSE '小王'
    END INTO v_name FROM DUAL;
    
    -- 生成随机的电话号码
    SELECT '1' || LPAD(TRUNC(DBMS_RANDOM.VALUE(100000000, 999999999)), 9, '0') INTO v_phone FROM DUAL;
    
    -- 执行插入操作
    INSERT INTO TB_CUSTOMER1 (name, phone, address)
    VALUES (v_name, v_phone, '成都大学');
  END LOOP;
  
  COMMIT;
END;
/

```

```
插入下单数据
-- 创建表
CREATE TABLE TB_ORDERS (
  customer VARCHAR2(50),
  pro_name VARCHAR2(50),
  time TIMESTAMP,
  num NUMBER
);

-- 插入数据
DECLARE
  v_customer VARCHAR2(10);
  v_pro_name VARCHAR2(50);
  v_time TIMESTAMP;
  v_num NUMBER;
BEGIN
  FOR i IN 1..10000 LOOP
    -- 生成随机的用户姓名
    SELECT CASE MOD(i, 3)
      WHEN 1 THEN '小明'
      WHEN 2 THEN '小红'
      ELSE '小王'
    END INTO v_customer FROM DUAL;
    
    -- 生成随机的商品名称
    SELECT '零食品种' || TO_CHAR(DBMS_RANDOM.VALUE(1, 1000)) INTO v_pro_name FROM DUAL;
    
    -- 生成随机的下单时间（精确到分钟）
    SELECT TO_TIMESTAMP('2023-01-01', 'YYYY-MM-DD') + INTERVAL '1' MINUTE * TRUNC(DBMS_RANDOM.VALUE(0, 365 * 24 * 60)) INTO v_time FROM DUAL;
    
    -- 生成随机的下单数量
    SELECT TRUNC(DBMS_RANDOM.VALUE(1, 100)) INTO v_num FROM DUAL;
    
    -- 执行插入操作
    INSERT INTO TB_ORDERS (customer, pro_name, time, num)
    VALUES (v_customer, v_pro_name, v_time, v_num);
  END LOOP;
  
  COMMIT;
END;
/
select * from TB_ORDERS

```

```
创建用户：


-- 创建管理员用户
CREATE USER MyAdmin01 IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE, DBA TO MyAdmin;

-- 创建普通用户
CREATE USER MyUser01 IDENTIFIED BY pwd;
GRANT CONNECT, RESOURCE TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_CLASS TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_CUSTOMER1 TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_ORDERS TO myUser01;
GRANT SELECT, INSERT, UPDATE, DELETE ON TB_COMMODITY TO myUser01;

```

```
-- 创建程序包体
CREATE OR REPLACE PACKAGE BODY ORDER_MANAGEMENT AS
  -- 创建订单存储过程的实现
  PROCEDURE create_order(
    p_customer_name IN VARCHAR2,
    p_product_name IN VARCHAR2,
    p_order_date IN DATE,
    p_quantity IN NUMBER
  ) AS
  BEGIN
    INSERT INTO TB_ORDERS (customer, pro_name, time, num)
    VALUES (p_customer_name, p_product_name, p_order_date, p_quantity);
    COMMIT;
  END create_order;

  -- 计算订单总金额存储过程的实现
  PROCEDURE calculate_order_total(
    p_order_id IN NUMBER,
    p_total_amount OUT NUMBER
  ) AS
  BEGIN
    SELECT num * total_price INTO p_total_amount
    FROM TB_ORDERS
    WHERE id = p_order_id;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      p_total_amount := 0;
  END calculate_order_total;
END ORDER_MANAGEMENT;
/

```

### 备份

```

备份方案设计：

数据库备份策略：

完全备份：每周执行一次完全备份，备份整个TB_ORDERS05表及其相关数据。
增量备份：每天执行增量备份，备份自上次备份以来发生的更改数据。
日志备份：定期备份数据库的事务日志，以便在需要时进行恢复操作。
备份方法：

使用Oracle提供的工具（如RMAN）进行备份操作，以保证备份的一致性和可靠性。
将备份文件存储在一个安全的位置，建议使用外部存储介质，如网络附加存储（NAS）、磁带库或云存储服务。
对备份文件进行加密，确保数据的机密性和安全性。
自动化备份过程：

使用脚本或自动化工具配置和管理备份过程，确保备份操作的一致性和准确性。
配置定时任务，自动执行备份策略，并记录备份过程的日志和报告。
监控备份任务的执行情况，及时处理备份失败或异常情况。
恢复测试：

定期进行数据库恢复测试，验证备份文件的可用性和恢复过程的有效性。
在测试环境中模拟不同的恢复场景，包括完全恢复和部分恢复，以确保备份数据的可靠性和完整性。
备份文件保留：

根据需求和可用存储空间设置合理的备份保留期限，以便可以恢复到指定的时间点。
定期清理过期的备份文件，释放存储空间。
监控和报警：

设置监控和报警机制，监视备份任务的运行情况和备份文件的完整性。
及时响应备份失败、备份文件损坏或其他异常情况，并采取必要的纠正措施。

```







